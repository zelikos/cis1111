// Patrick Csikos
// 2020-02-25
// Relational
// Determines which apartment to rent

#include <cmath>
#include <iostream>
#include <string>
using namespace std;

int main () {
    // named by location of apartment + number of bedrooms
    const double BY_RIVER_ONE = 1225, BY_RIVER_TWO = 1750, // apartment cost before utilities
                 CLOSE_RIVER_ONE = 700, CLOSE_RIVER_TWO = 1200,
                 FAR_RIVER_ONE = 500, FAR_RIVER_TWO = 800,
                 PAYMENT_PRC = 0.4, // percentage of income for rent and utilities
                 UTILITIES = 50; // for by-the-river apartments
    
    bool affordable = false;
    double payment_amount;
    int monthly_income, num_bedrooms, num_roommates;
    string can_afford = "You can afford an apartment ", // prefix to "can afford" output
           // 
           by_river = "by the river. Utilities are not included.\n",
           close_river = "close to the river. Utilities are included.\n",
           far_river = "far from the river. Utilities are included.\n";
    
    cout << "Welcome to What it Takes to Live Downtown, Rental Calculator\n" << endl;
    
    cout << "Enter your monthly income: ";
    cin >> monthly_income;
    
    if (monthly_income <= 0) {
        cout << "You can't afford any apartment without an income." << endl;
    } else {
        payment_amount = monthly_income * PAYMENT_PRC;
        cout << "Enter how many bedrooms you want (1 or 2): ";
        cin >> num_bedrooms;
    
        switch (num_bedrooms) {
            case 1:
                if (payment_amount >= BY_RIVER_ONE + UTILITIES) {
                    affordable = true;
                    can_afford += by_river;
                } else if (payment_amount >= CLOSE_RIVER_ONE) {
                    affordable = true;
                    can_afford += close_river;
                } else if (payment_amount >= FAR_RIVER_ONE) {
                    affordable = true;
                    can_afford += far_river;
                } else if (payment_amount < FAR_RIVER_ONE) {
                    affordable = false;
                    // number of roommates equal to lowest-cost apartment divided by the payment amount, minus the payment from the user
                    num_roommates = (ceil(FAR_RIVER_ONE / payment_amount) - 1);
                }
                break;
            case 2:
                if (payment_amount >= BY_RIVER_TWO + UTILITIES) {
                    affordable = true;
                    can_afford += by_river;
                } else if (payment_amount >= CLOSE_RIVER_TWO) {
                    affordable = true;
                    can_afford += close_river;
                } else if (payment_amount >= FAR_RIVER_TWO) {
                    affordable = true;
                    can_afford += far_river;
                } else if (payment_amount < FAR_RIVER_TWO) {
                    affordable = false;
                    num_roommates = (ceil(FAR_RIVER_TWO / payment_amount) - 1);
                }
                break;
            default:
                cout << "Number of bedrooms must be 1 or 2." << endl;
        }
        
        if (affordable) {
            cout << can_afford << endl;
        } else if (!affordable) {
            cout << "If you want to live downtown, you will need " << num_roommates << " roomate(s).\n" << endl;
        }
    }
    
    
    char ch;
    cout << "Press Enter to continue...";
    cin.ignore();
    cin.get(ch);
    return 0;
}
