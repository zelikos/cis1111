// Chapter 5 - Loop Examples
// Date:  /27/2017 
// Description:  These are examples of the three (3) loops in the
// C++ programming language.

#include <iostream>
#include <string>
#include <iomanip>
using namespace std;

int main()
{
	// The first loop below is a while loop

	int cnt;   // This is the loop control variable, also called the loop counter

	cnt = 1;   // Initialize loop counter outside the loop

	while (cnt < 6)  // Loop Control Statement (loop counter)
	{
		cout << "This is loop execution # " << cnt << endl;
		cnt = cnt + 1;  // Modify loop counter INSIDE the loop
	}  // end while loop

	cout << endl;
/**/
	// This while loop allows you to enter several test scores.  After all scores
	// have been entered:
	// - the loop is terminated and
	// - the average test score is computed.

	string GO;  // This Sentinel Value will control loop execution.  
	double score, totalOfScores;
	int count = 0;  // To count thenumber of test scores entered
	totalOfScores = 0;

	GO = "Y";

	while (GO == "Y")  // Loop Control Statement (sentinel value)
	{
		cout << "Enter your test score: ";
		cin >> score;

		totalOfScores = totalOfScores + score;
		count++;
		cout << "Any more test scores?  Enter Y to continue, anything else to stop. ";
		cin >> GO;
	}  // end while loop

	cout << endl;
	cout << "The Average Score is " << (totalOfScores / count) << endl;
	cout << endl;

	// This is a do while loop.  
	// The body of this loop will always execute at least one time. 

	int number = 6;

	do
	{
		cout << "do while loop was executed.  ";
		number++;
	} while (number <= 5); // Loop Control Variable

	cout << endl << endl;

	// This is a for loop.  It is a counter-controlled loop.
	// Loop counter initialization, the expression to text, and loop counter update are 
	// all separate statements enclosed in parenthesis.

	int i, j;
	j = 5;

	for (i = 0; i < j; i++)
	{
		cout << (i + 1) << " Potato" << endl;
	}

	cout << endl << endl;
	cout << "That's all folks!!" << endl;
/**/
	return 0;
}
