// Chapter 5A
// Loops and Files
// Today's Date
//
//  This program has the user input2 positive integers, first and last
//  and then finds the mean of the consecutive integers from first to last

// YOUR NAME HERE

#include <iostream>
using namespace std;


int main()
{
  int first, last;
  int total = 0;   // total holds the sum of the first n positive numbers
    int number;      // the amount of numbers
    float mean;      // the average of the first n positive numbers

    cout << "Please enter the beginning positive integer" << endl;
    cin >> first;

  cout << "Please enter the ending positive integer" << endl;
    cin >> last;

    if (first > 0  && last >= first)
    {
 // Write the for loop statement here.
        {
          total = total + number;
        }  // curly braces are optional since there is only one statement

        mean = float(total) / (last - first + 1);  // note the use of the typecast
                                            // operator here
    cout << "The mean average of the consecutive positive integers " << endl
       << "from " << first << " to " << last << " is " << mean << endl;

    }
    else
        cout << "Invalid input - integers must be positive and last must be >= to first" << endl;

   return 0;
}
