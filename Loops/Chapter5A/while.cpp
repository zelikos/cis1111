// Chapter 5A
// Loops and Files
// Today's Date
//
// PLACE YOUR NAME HERE

#include <iostream>
using namespace std;

// Exercise 1: This program is not very user friendly. Run it a few times and 
// explain why it is not user friendly.

// Exercise 2: Add  to the  code so  that  the  program is  more user friendly.

// Exercise 3: How would this code affect the execution of the program if the
// while loop is replaced by a do-while loop? Try it and  see.  Rewrite the while
// loop as a do-while loop  just above the system("pause") statement below.

int main()
{
  char letter = 'a';

    while (letter != 'x')
    {
           cout << "Please enter a letter" << endl;
           cin >> letter;
           cout << "The letter you entered is " << letter << endl;
    }

// Exercise 3 here - Rewrite above loop as a do-while loop

    system("pause");
    return 0;
}
