// Chapter 5A
// Loops and Files
// Today's Date
//
// This program finds the average time spent doing homework by a student
// each day over a specified number of days.

// PLACE YOUR NAME HERE

#include <iostream>
using namespace std;

//Exercise 1: Note that the inner loop of this program is always executed
// exactly three times�once for each day of the long weekend.
// Modify the code so that the inner loop iterates n times, where n is a
// positive integer input by the user.  In other words, let the user decide
// how many days to consider just as they choose how many students to consider.
//
//Exercise 2: Modify the program from Exercise 1 so that it also finds the
// average number of hours per day that a given student studies biology as well
// as programming.  For each given student include two prompts, one for each
// subject.  Have the program print out which subject the student, on average,
// spent the most time on.

int main()
{
  int numStudents;
  int numDays;
  double numHours, total, average;
  int student, day = 0;     // these are the counters for the loops

  cout << "This program will find the average number of hours a day"
         << " that a student spent \n\n";
         
  cout << "How many students are there ?" << endl;
  cin >> numStudents;
  
  cout << "How many days did the student study ?" << endl << endl;
  cin >> numDays;

  // Code a for loop statement below for the number of students  
    {
      total = 0;
      
  // Modify the for loop below to use the number of days entered by the user.
      for (day = 1; day <= 3; day++)
      {
        cout << "Please enter the number of hours worked by student "
         << student <<" on day " << day << "." << endl;
            cin >> numHours;

            total = total + numHours;
      }
  // Code statement(s) here to determine the average number of hours the
  // student studied.

    cout << endl;
    cout << "The average number of hours per day spent programming by "
         << "student " << student << " is " << average
          << endl << endl << endl;
  }
  system("pause");
  return 0;

}
