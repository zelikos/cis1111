// Chapter 5A
// Loops and Files
// Today's Date
//
// This program illustrates the use of a sentinel in a while loop.
// The user is asked for monthly rainfall totals until a sentinel
// value of -1 is entered. Then the total rainfall is displayed

// PLACE YOUR NAME HERE

#include <iostream>
using namespace std;

//Exercise: Complete the program below by filling in the code described in the
// comments so that it will perform the indicated task.

//Exercise:  Run the program several times with various input.  Record your
// results.  Are they correct?  What happens if you enter �1 first?
// What  happens if you enter only values of 0 for one or more months?
// Is there any numerical data that you should not enter?

//Exercise: What is the purpose of the following code in the program?
//
// if (month == 1)
//     cout << "No data has been entered" << endl;

int main()
{

// Fill in the code to define an integer and initialize to 1 the variable "month"

  float total = 0;
  float rain;

  cout << "Enter the total rainfall in inches for month " << month << endl;
  cout << "Enter -1 when you are finished" << endl;

// Fill in the code to read in the value for rain


// Fill in the code to start a while loop that iterates
// while rain does not equal -1

  {

// Fill in the code to update "total" by adding rain to it
// Fill in the code to increment month by one

      cout << "Enter the total rainfall in inches for month "
           << month << endl;
      cout << "Enter -1 when you are finished" << endl;

// Fill in the code to read in the value for rain

  }

  if (month == 1)
    cout << "No data has been entered" << endl;

  else
    cout << "The total rainfall for the " << month-1
         << " months is "<< total << " inches." << endl;

   system ("pause");
   return 0;
}
