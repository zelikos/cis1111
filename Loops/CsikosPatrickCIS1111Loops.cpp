// Patrick Csikos
// 2020-03-29
// Loops
// Produces a sales report for the "CPlusPlus & LandMinusMinus company"

#include <iomanip>
#include <iostream>
#include <string>
using namespace std;

int main () {
    const double TAX_RATE = 0.30;
    const string DIV1 = "East Coast", DIV2 = "Mid West", DIV3 = "West Coast",
          SALES_PROMPT = "Enter sales for ", EXPENSES_PROMPT = "Enter expenses for ";

    char running = 'y';
    double sales, expenses, div1_total, div2_total, div3_total, total_for_month, // Sales/expenses for each month
           average_sales, total_sales = 0, highest_sales = 0; // Sales numbers across all months
    int num_months = 0;
    string month, highest_month;
    
    cout << fixed << showpoint << setprecision(2);
    
    do {
        cout << "Enter name of month: ";
        cin >> month;
        
        cout << SALES_PROMPT << DIV1 << ": ";
        cin >> sales;
        while (sales < 0) { // validating input
            cout << "Error: Sales must be zero or greater.\n";
            cout << "Re-enter sales: ";
            cin >> sales;
        }
        
        cout << EXPENSES_PROMPT << DIV1 << ": ";
        cin >> expenses;
        while (expenses < 0) {
            cout << "Error: Expenses must be zero or greater.\n";
            cout << "Re-enter expenses: ";
            cin >> expenses;
        }

        div1_total = sales - expenses; // calculates subtotal
        div1_total -= div1_total * 0.30; // subtracts tax


        cout << SALES_PROMPT << DIV2 << ": ";
        cin >> sales;
        while (sales < 0) {
            cout << "Error: Sales must be zero or greater.\n";
            cout << "Re-enter sales: ";
            cin >> sales;
        }
        
        cout << EXPENSES_PROMPT << DIV2 << ": ";
        cin >> expenses;
        while (expenses < 0) {
            cout << "Error: Expenses must be zero or greater.\n";
            cout << "Re-enter expenses: ";
            cin >> expenses;
        }

        div2_total = sales - expenses;
        div2_total -= div2_total * 0.30;
        

        cout << SALES_PROMPT << DIV3 << ": ";
        cin >> sales;
        while (sales < 0) {
            cout << "Error: Sales must be zero or greater.\n";
            cout << "Re-enter sales: ";
            cin >> sales;
        }
        
        cout << EXPENSES_PROMPT << DIV3 << ": ";
        cin >> expenses;
        while (expenses < 0) {
            cout << "Error: Expenses must be zero or greater.\n";
            cout << "Re-enter expenses: ";
            cin >> expenses;
        }

        div3_total = sales - expenses;
        div3_total -= div3_total * 0.30;

        
        cout << endl << "CPlusPlus & LandMinusMinus Net Sales for " << month << endl;
        cout << "\t(each asterisk represents $1,000)" << endl;

        // Displays total sales for each division
        cout << left << setw(10) << DIV1 << " $ " << right << setw(10) << div1_total;
        if (div1_total < 1000) {
            cout << " - " << endl;
        } else {
            int ast = div1_total / 1000;
            for (int i = 1; i <= ast; i++) {
                cout << " * ";
            }
            cout << endl;
        }

        cout << left << setw(10) << DIV2 << " $ " << right << setw(10) << div2_total;
        if (div2_total < 1000) {
            cout << " - " << endl;
        } else {
            int ast = div2_total / 1000;
            for (int i = 1; i <= ast; i++) {
                cout << " * ";
            }
            cout << endl;
        }

        cout << left << setw(10) << DIV3 << " $ " << right << setw(10) << div3_total;
        if (div3_total < 1000) {
            cout << " - " << endl;
        } else {
            int ast = div3_total / 1000;
            for (int i = 1; i <= ast; i++) {
                cout << " * ";
            }
            cout << endl;
        }
        // Really looking forward to functions


        total_for_month = div1_total + div2_total + div3_total;

        // Checks if current month had the highest sales numbers
        if (total_for_month > highest_sales) {
            highest_sales = total_for_month;
            highest_month = month;
        }

        total_sales += total_for_month;
        num_months++;
        
        cout << endl << "Enter Y to process another month: ";
        cin >> running;
        cout << endl;
        
    } while (running == 'y' || running == 'Y');

    average_sales = total_sales / num_months;
    cout << endl << "The average net sales per month are $" << average_sales << endl;
    cout << highest_month << " had the higest net sales, with $" << highest_sales << endl;


    char ch;
    cout << endl << "Press Enter to continue...";
    cin.ignore();
    cin.get(ch);
    return 0;
}
