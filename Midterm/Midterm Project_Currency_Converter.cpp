// Patrick Csikos
// 2020-02-27
// Midterm Project - Foreign Currency Converter
// Converts USD to or from a selection of foreign currencies

#include <iomanip>
#include <iostream>
#include <string>
using namespace std;

int main () {
    // Rates of USD to other currencies, as of March 1 2020
    const double TO_CAD = 1.34, TO_EUR = 0.91, TO_GBP = 0.78, TO_AUD = 1.53, TO_JPY = 108.09;
    const int CAD = 1, EUR = 2, GBP = 3, AUD = 4, JPY = 5;
    const string INVALID_SELECTION = "Invalid selection. Please re-run the program and select a valid option.\n";

    double initial_value, result; // initial value of currency, result of conversion
    int choice, usd_to_foreign, foreign_to_usd;

    cout << fixed << showpoint << setprecision(2);

    cout << "USD to Foreign Currency\n" << endl;
    
    cout << "[1] Convert USD to foreign currency" << endl;
    cout << "[2] Convert foreign currency to USD" << endl;
    cout << "[3] Quit\n" << endl;
    
    cout << "Please select an option: ";
    cin >> choice;
    
    // same currency choices for both options above, as USD is the common factor
    cout << endl;
    cout << "[1] Canadian Dollars (CAD)\n";
    cout << "[2] Euros (EUR)\n";
    cout << "[3] British Pounds (GBP)\n";
    cout << "[4] Australian Dollar (AUD)\n";
    cout << "[5] Japanese Yen (JPY)\n" << endl;
            
    switch (choice) {
        case 1:
            cout << "Please select currency to convert USD to: ";
            cin >> usd_to_foreign;
            switch (usd_to_foreign) {
                case CAD:
                    cout << "Please enter USD amount to convert to CAD: ";
                    cin >> initial_value;
                    result = initial_value * TO_CAD;
                    cout << endl << initial_value << " USD is " << result << " CAD.\n";
                    break;
                case EUR:
                    cout << "Please enter USD amount to convert to EUR: ";
                    cin >> initial_value;
                    result = initial_value * TO_EUR;
                    cout << endl << initial_value << " USD is " << result << " EUR.\n";
                    break;
                case GBP:
                    cout << "Please enter USD amount to convert to GBP: ";
                    cin >> initial_value;
                    result = initial_value * TO_GBP;
                    cout << endl << initial_value << " USD is " << result << " GBP.\n";
                    break;
                case AUD:
                    cout << "Please enter USD amount to convert to AUD: ";
                    cin >> initial_value;
                    result = initial_value * TO_AUD;
                    cout << endl << initial_value << " USD is " << result << " AUD.\n";
                    break;
                case JPY:
                    cout << "Please enter USD amount to convert to JPY: ";
                    cin >> initial_value;
                    result = initial_value * TO_JPY;
                    cout << endl << initial_value << " USD is " << result << " JPY.\n";
                    break;
                default:
                    cout << INVALID_SELECTION;
            }
            break;
        case 2:
            cout << "Please select currency to convert to USD: ";
            cin >> foreign_to_usd;
            switch (foreign_to_usd) {
                case CAD:
                    cout << "Please enter CAD amount to convert to USD: ";
                    cin >> initial_value;
                    result = initial_value / TO_CAD;
                    cout << endl << initial_value << " CAD is " << result << " USD.\n";
                    break;
                case EUR:
                    cout << "Please enter EUR amount to convert to USD: ";
                    cin >> initial_value;
                    result = initial_value / TO_EUR;
                    cout << endl << initial_value << " EUR is " << result << " USD.\n";
                    break;
                case GBP:
                    cout << "Please enter GBP amount to convert to USD: ";
                    cin >> initial_value;
                    result = initial_value / TO_GBP;
                    cout << endl << initial_value << " GBP is " << result << " USD.\n";
                    break;
                case AUD:
                    cout << "Please enter GBP amount to convert to AUD: ";
                    cin >> initial_value;
                    result = initial_value / TO_AUD;
                    cout << endl << initial_value << " AUD is " << result << " USD.\n";
                    break;
                case JPY:
                    cout << "Please enter JPY amount to convert to USD: ";
                    cin >> initial_value;
                    result = initial_value / TO_JPY;
                    cout << endl << initial_value << " JPY is " << result << " USD.\n";
                    break;
                default:
                    cout << INVALID_SELECTION;
            }
            break;
        case 3:
            cout << "Exiting program.\n";
            break;
        default:
            cout << INVALID_SELECTION;
    }
    
    char ch;
    cout << endl << "Press Enter to continue...";
    cin.ignore();
    cin.get(ch);
    return 0;
}
