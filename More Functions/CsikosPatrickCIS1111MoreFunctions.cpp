// Patrick Csikos
// 2020-04-19
// More Functions
// Calculates and displays a baseball player's batting average and proficiency

#include <iomanip>
#include <iostream>
#include <string>
using namespace std;

void end_pause ();


double calculate_average (string);
string batting_proficiency (double);

int main () {
    char processing;
    double batting_average;
    string player_name, player_proficiency;

    do {
        cout << "Enter the player's full name: ";
        getline (cin, player_name);

        batting_average = calculate_average (player_name);

        player_proficiency = batting_proficiency (batting_average);

        cout << fixed << showpoint << setprecision(3);

        cout << player_name << "'s batting average is " << batting_average
             << " and thus his proficiency is: " << player_proficiency << endl;

        cout << "\nEnter Y to process another player, any other key to quit: ";
        cin >> processing;
        cin.ignore(1000, '\n'); // Clears input buffer for next player entry
    } while (processing == 'Y' || processing == 'y');

    end_pause ();
    return 0;
}

double calculate_average (string player) {
    char more_games;
    double batting_average;
    int game_count = 1, hits, at_bats, total_hits = 0, total_at_bats = 0;

    do {
        do {
            cout << "Enter " << player << "'s hits for game #" << game_count << ": ";
            cin >> hits;
            if (hits < 0) {
                cout << "Number of hits must be zero or greater.\n";
            }
        } while (hits < 0);

        do {
            cout << "Enter " << player << "'s at-bats for game #" << game_count << ": ";
            cin >> at_bats;
            if (at_bats < hits) {
                cout << "Number of at-bats must be equal to or higher than number of hits.\n";
            }
        } while (at_bats < hits); // Since hits must be >= 0, at_bats will be too if >= hits
        
        game_count++;
        total_hits += hits;
        total_at_bats += at_bats;

        cout << "\nEnter Y to add another game's stats, any other key to quit: ";
        cin >> more_games;

    } while (more_games == 'Y' || more_games == 'y');

    batting_average = (total_hits * 1.0) / total_at_bats;
    return batting_average;
}

string batting_proficiency (double batting_average) {
    string proficiency;

    if (batting_average < 0.250) {
        proficiency = "Minor Leaguer";
    } else if (batting_average < 0.300) {
        proficiency = "All Star";
    } else if (batting_average < 0.400) {
        proficiency = "Hall of Famer";
    } else {
        proficiency = "King of Baseball";
    }

    return proficiency;
}

// Pauses program so user can read terminal output, without relying on OS-specific system calls
void end_pause () {
    char ch;
    cout << endl << "Press Enter to continue...";
    cin.ignore();
    cin.get(ch);
}