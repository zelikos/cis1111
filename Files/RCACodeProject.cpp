#include <fstream>
#include <iostream>
using namespace std;

int main () {
    ifstream numberList;
    int average, count = 0, num, total = 0;
    
    numberList.open("Random.txt");
    
    if (numberList) {
        while (numberList >> num) {
            count++;
            total += num;
        }
        
        average = (total / count);
        cout << "Number of numbers: " << count << endl;
        cout << "Sum of the numbers: " << total << endl;
        cout << "Average of the numbers: " << average << endl;
    } else {
        cout << "Unable to open file.";
    }
    
    numberList.close();
    return 0;
}
