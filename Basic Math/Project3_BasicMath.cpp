// Patrick Csikos
// 2020-01-24
// Basic Math
// Calculates the cost for a company to upgrade its computers

#include <iostream>
#include <string>
using namespace std;

int main() {
    const string COMPANY = "Sinclair Industries";
    // const int NUM_TABLETS = 5, NUM_DESKTOPS = 5;
    const int NUM_TABLETS = 7, NUM_DESKTOPS = 3;

    int numComputers = NUM_TABLETS + NUM_DESKTOPS;
    double costPerTablet = 320.00, costPerDesktop = 800.00;
    double salesTax = 0.075, discount = 0.10, financeCharge = 0.15;
    // Calculates total costs for tablets and desktop PCs
    double tabletTotal = costPerTablet * NUM_TABLETS,
           desktopTotal = costPerDesktop * NUM_DESKTOPS;
    
    // Calculates subtotal for all tablets and desktop PCs purchased
    double subTotal = tabletTotal + desktopTotal;

    double cashPrice = subTotal - (subTotal * discount), // Calculates price with cash purchase
           financePrice = subTotal + (subTotal * financeCharge), // Calculates price with financed purchase
           cashTotal, financeTotal, avgForCash, avgForFinance;

    cout << "Name of Company: " << COMPANY << endl
         << "Number of tablets to purchase: " << NUM_TABLETS << endl
         << "Number of desktop PCs to purchase: " << NUM_DESKTOPS << endl 
         << endl;

    cout << "Invoice for " << COMPANY << endl << endl;

    cout << "Cost for " << NUM_TABLETS << " Tablets: $" << tabletTotal 
         << endl
         << "Cost for " << NUM_DESKTOPS << " Desktop PCs: $" 
         << desktopTotal << endl
         << endl;

    // Calculates total cash price plus tax for the computers
    cashTotal = cashPrice + (cashPrice * salesTax);
    // Calculates average cost per computer for cash purchase
    avgForCash = cashTotal / numComputers;

    cout << "If paying cash" << endl
         << "Subtotal: $" << cashPrice << endl
         << "Tax: $" << cashPrice * salesTax << endl
         << "Total: $" << cashTotal << endl
         << "The average cost per computer is $" << avgForCash
         << endl << endl;
    
    // Calculates total price plus tax for financed purchase
    financeTotal = financePrice + (financePrice * salesTax);
    // Calculates average cost per computer for financed purchase
    avgForFinance = financeTotal / numComputers;

    cout << "If financing" << endl
         << "Subtotal: $" << financePrice << endl
         << "Tax: $" << financePrice * salesTax << endl
         << "Total: $" << financeTotal << endl
         << "The average cost per computer is $" << avgForFinance
         << endl << endl;
    
    cout << "By paying cash you would save $" << avgForFinance - avgForCash
         << " per computer" << endl;
    
    return 0;
}