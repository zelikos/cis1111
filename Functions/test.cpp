// Patrick Csikos
// Date
// Program Name
// Program description

#include <iostream>
using namespace std;

void end_pause ();
void printLarger(int, int);

int main () {
    printLarger(20, 30);

    end_pause ();
    return 0;
}

void printLarger(int num1, int num2) {
    if (num1 > num2) {
        cout << num1 << endl;
    } else if (num1 < num2) {
        cout << num2 << endl;
    }
}

// Pauses program so user can read terminal output, without relying on OS-specific system calls
void end_pause () {
    char ch;
    cout << endl << "Press Enter to continue...";
    cin.ignore();
    cin.get(ch);
}