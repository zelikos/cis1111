// Patrick Csikos
// 2020-04-11
// Functions
// Menu-driven program for food purchases at a restaurant

#include <iomanip>
#include <iostream>
using namespace std;

void end_pause ();

void display_menu ();
double calculate_bill (double, double, double);
void display_bill (double, double, double, double);
void display_change (double, double);

int main () {
                 // Prices for menu items
    const double GYRO = 7.00, MOUS = 8.50, SALAD = 3.75, DOLMA = 2.00, SODA = 2.80, COFFEE = 1.00, WATER = 2.00,
                 TAX = 0.065, TIP = 0.2;

    double payment = -1, running_total = 0, total;
    char choice;

    cout << fixed << showpoint << setprecision(2);

    display_menu();

    do {
        cout << endl << "Select menu item: ";
        cin >> choice;
        switch (choice) {
            case '1':
                running_total += GYRO;
                cout << "Gyro added." << endl;
                break;
            case '2':
                running_total += MOUS;
                cout << "Moussaka added." << endl;
                break;
            case '3':
                running_total += SALAD;
                cout << "Salad added." << endl;
                break;
            case '4':
                running_total += DOLMA;
                cout << "Dolma added." << endl;
                break;
            case '5':
                running_total += SODA;
                cout << "Soda added." << endl;
                break;
            case '6':
                running_total += COFFEE;
                cout << "Coffee added." << endl;
                break;
            case '7':
                running_total += WATER;
                cout << "Water added." << endl;
                break;
            case '8':
                cout << "Calculating bill..." << endl << endl;
                break;
            default:
                cout << "Invalid selection." << endl;
        }
    } while (choice != '8');

    total = calculate_bill (running_total, TAX, TIP);

    display_bill (running_total, total, TAX, TIP);

    while (payment < total) {
        cout << "Enter payment amount: ";
        cin >> payment;
        if (payment < total) {
            cout << "Payment must be equal to or higher than amount due." << endl;
        }
    }

    display_change (total, payment);

    end_pause ();
    return 0;
}

void display_menu () {
    cout << "Greek Islands Menu" << endl;
    cout << left << setw(16) << "1 - Gyro" << right << setw(6) << "$7.00" << endl;
    cout << left << setw(16) << "2 - Moussaka" << right << setw(6) << "$8.50" << endl;
    cout << left << setw(16) << "3 - Salad" << right << setw(6) << "$3.75" << endl;
    cout << left << setw(16) << "4 - Dolma" << right << setw(6) << "$2.00" << endl;
    cout << left << setw(16) << "5 - Soda" << right << setw(6) << "$2.80" << endl;
    cout << left << setw(16) << "6 - Coffee" << right << setw(6) << "$1.00" << endl;
    cout << left << setw(16) << "7 - Water" << right << setw(6) << "$2.00" << endl; // water costs more than coffee?
    cout << left << setw(16) << "8 - End order" << endl;
}

double calculate_bill (double subtotal, double tax, double tip) {
    double total = subtotal + (subtotal * tax) + (subtotal * tip);
    return total;
}

void display_bill (double bill, double totBill, double tax, double tip) {
    cout << left << setw(12) << "Subtotal: " << right << setw(6) << bill << endl;
    cout << left << setw(12) << "Tax: " << right << setw(6) << (bill * tax) << endl;
    cout << left << setw(12) << "Tip: " << right << setw(6) << (bill * tip) << endl;
    cout << left << setw(12) << "\nTotal amount due: " << right << setw(6) << totBill << endl << endl;
}

void display_change (double totBill, double amtTendered) {
    double change = amtTendered - totBill;
    cout << left << setw(12) << "Change due: " << right << setw(6) << change << endl;
}

// Pauses program so user can read terminal output, without relying on OS-specific system calls
void end_pause () {
    char ch;
    cout << endl << "Press Enter to continue...";
    cin.ignore();
    cin.get(ch);
}