// Patrick Csikis
// 2020-02-19
// Decisions
// Processes applications for events at a dinner club with restrictions

#include <iostream>
#include <string>
using namespace std;

int main () {
    void end_prompt ();
    const int MAX_OCCUPANCY = 100, MAX_EVENT_LENGTH = 360; // event length in minutes
    
    int event_length, num_guests;
    string event_name, customer_name;
    
    cout << "Enter name of event: ";
    getline(cin, event_name);
    
    cout << "Enter customer's full name: ";
    getline(cin, customer_name);
    
    cout << "Enter number of guests: ";
    cin >> num_guests;
    
    if (num_guests > MAX_OCCUPANCY) {
        cout << "Your number of guests is greater than our club's capacity of 100.\n";
        cout << "Please decrease the number of guests, or choose another venue.\n";
        end_prompt ();
        return 0;
    }
    
    cout << "Enter length of event (in minutes): ";
    cin >> event_length;

    end_prompt ();
    return 0;
}

void end_prompt () {
    char ch;
    cout << endl << "Press Enter to continue...";
    cin.ignore();
    cin.get(ch);
}
