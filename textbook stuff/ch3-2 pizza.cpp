#include <cmath>
#include <iostream>
using namespace std;

int main () {
    const double PI = 3.14159;
    const double SLICE_AREA = 14.125;
    double area, diamater, radius;
    int slices;
    
    cout << "Enter pizza diamater: ";
    cin >> diamater;
    
    radius = diamater / 2;
    area = PI * pow(radius, 2.0);
    slices = area / SLICE_AREA;
    
    cout << slices << " slices" << endl;
    return 0;
}
