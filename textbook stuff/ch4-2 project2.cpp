#include <iostream>
using namespace std;

int main () {
    const double PI = 3.14159;
    double height, length, radius, width;
    int selection;
    
    cout << "Geometry Calculator\n" << endl;
    cout << "1. Calculate the area of a Circle\n";
    cout << "2. Calculate the area of a Rectangle\n";
    cout << "3. Calculate the area of a Triangle\n";
    cout << "4. Quit\n" << endl;
    cout << "Enter your choice (1-4): ";
    
    cin >> selection;
    cout << endl;
    
    switch (selection) {
        case 1:
            cout << "Enter the circle's radius: ";
            cin >> radius;
            if (radius >= 0) {
                cout << "The area is: " << (PI * (radius * radius)) << endl;
            } else {
                cout << "The radius can not be less than zero.\n";
            }
            break;
        case 2:
            cout << "Enter the rectangle's length and width: ";
            cin >> length >> width;
            if (length < 0 || width < 0) {
                cout << "Only enter positive values for length and width.\n";
            } else {
                cout << "The area is: " << (length * width) << endl;
            }
            break;
        case 3:
            cout << "Enter the triangle's base and height: ";
            cin >> length >> height;
            if (length < 0 || height < 0) {
                cout << "Only enter positive values for base and height.\n";
            } else {
                cout << "The area is: " << (length * height * 0.5) << endl;
            }
            break;
        case 4:
            cout << "Exiting.\n";
            break;
        default:
            cout << "The valid choices are 1 through 4. Run the program again and select one of those.\n";
    }
    return 0;
}
