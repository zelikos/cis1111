// Patrick Csikis
// 2020-01-18
// DefiningVariables
// Demonstrates defining variables with C++

#include <iostream>
using namespace std;

int main() {
    string myName = "Patrick Csikos";
    string major = "Software Engineering";
    int creditHours = 6;
    double tuitionRate = 99.03;

    cout << "My name is " << myName << endl;
    cout << "I am majoring in " << major << endl;
    cout << "I am taking " << creditHours << " credit hours" << endl;
    cout << "I am paying $" << tuitionRate << " per credit hour" << endl;

    return 0;
}