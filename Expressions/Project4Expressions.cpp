// Patrick Csikis
// 2020-02-05
// Expressions
// Calculates the cost to cater a dinner

#include <iomanip>
#include <iostream>
#include <cmath> // Needed for ceil() function
#include <string>
using namespace std;

int main () {
    const double COST_PER_MIN = 0.50, COST_PER_HOUR = 15.0, COST_PER_DINNER = 31.80,
                 DEPOSIT_RATE = 0.25;

    double total_cost, cost_per_guest, server_cost, food_cost, deposit_amount;
    string event_name, customer_name;
    int dinner_time, dinner_hours, dinner_mins, num_guests, num_servers;
    char ch;

    // Ensures dollar amounts are displayed properly
    cout << fixed << showpoint << setprecision(2);

    cout << "Enter name of event: ";
    getline(cin, event_name);

    cout << "Enter full name of customer: ";
    getline(cin, customer_name);

    cout << "Enter number of guests: ";
    cin >> num_guests;

    cout << "Enter length of event, in minutes: ";
    cin >> dinner_time;
    cout << endl << endl;


    num_servers = ceil(num_guests / 15.0); // Divided by double so result can round properly

    // Separates hours of the dinner's time from sub-hour minutes
    dinner_hours = dinner_time / 60, dinner_mins = dinner_time % 60;

    server_cost = num_servers * ((dinner_hours * COST_PER_HOUR) + (dinner_mins * COST_PER_MIN));
    food_cost = COST_PER_DINNER * num_guests;
    total_cost = server_cost + food_cost;
    cost_per_guest = total_cost / num_guests;
    deposit_amount = total_cost * DEPOSIT_RATE;


    cout << event_name << endl;
    cout << "Event estimate for " << customer_name << endl << endl;

    cout << "Number of servers: " << num_servers << endl;

    // setw value == 40 minus the number of characters in each string
    cout << "Cost of servers: " << setw(23) << server_cost << endl;
    cout << "Cost for food: " << setw(25) << food_cost << endl;
    cout << "Average cost per guest: " << setw(16) << cost_per_guest << endl;
    cout << "Total cost: " << setw(28) << total_cost << endl << endl;
    
    cout << "A 25% deposit is required to reserve the event" << endl;
    cout << "Deposit amount: " << setw(24) << deposit_amount << endl;


    // Used as cross-platform alternative to 'system("pause")`
    cout << endl << "Press any key to continue . . .";
    cin.ignore();
    cin.get(ch);
    return 0;
}