// Patrick Csikos
// 2020-04-30
// Final Project
// To Do List application

#include <fstream>
#include <iomanip>
#include <iostream>
#include <vector>
using namespace std;

void end_pause ();

void display_menu (vector<string> &todo_list);

void add_task (vector<string> &todo_list);
void remove_task (vector<string> &todo_list);
void clear_tasks (vector<string> &todo_list);
void export_list (vector<string> &todo_list);
void import_list (vector<string> &todo_list);


int main () {
    vector<string> todo_list;

    display_menu (todo_list);

    end_pause ();
    return 0;
}

void display_menu (vector<string> &todo_list) {
    char choice;

    cout << endl;

    if (todo_list.size() == 0) {
        cout << "Nothing to do" << endl;
        cout << "\nEnter I to import an existing list from a file" << endl;
    } else {
        for (int i = 0; i < todo_list.size(); i++) {
            cout << "[" << i+1 << "] " << todo_list[i] << endl;
        }
        cout << "\nEnter E to export the current list to a file" << endl;
    }

    cout << "Enter A to add a task, R to remove a task, or C to clear tasks" << endl;
    cout << "Enter Q to exit" << endl;
    
    cout << "\n> ";
    cin.get(choice);

    switch (choice) {
        case 'A':
        case 'a':
            add_task (todo_list);
            break;
        case 'R':
        case 'r':
            remove_task (todo_list);
            break;
        case 'C':
        case 'c':
            cin.ignore();
            clear_tasks (todo_list);
            break;
        case 'E':
        case 'e':
            export_list (todo_list);
            break;
        case 'I':
        case 'i':
            import_list (todo_list);
            break;
        case 'Q':
        case 'q':
            cout << "Exiting..." << endl;
            break;
        default:
            cout << "Invalid selection." << endl;
            cin.clear();
            cin.ignore();
            display_menu (todo_list);
            break;
    }
}

void add_task (vector<string> &todo_list) {
    char choice;
    string new_task;

    cout << "New task: ";
    cin.ignore(); // prevents accidental blank entry
    getline (cin, new_task);
    todo_list.push_back(new_task);
    cout << "Task added." << endl;

    display_menu (todo_list);
}

void remove_task (vector<string> &todo_list) {
    int task_num;

    if (todo_list.size() == 0) {
        cout << "No tasks listed." << endl;
    } else {
        cout << "Enter task # to remove: ";
        cin >> task_num;

        if (task_num > todo_list.size() || task_num < 1) {
            cout << "Selected number is not on list." << endl;
        } else {
            todo_list.erase(todo_list.begin() + task_num-1); // removes specified entry from vector
            cout << "Task #" << task_num << " removed." << endl;
        }
    }

    // clears input buffer in case of wrong typed input
    cin.clear();
    cin.ignore(1000, '\n');

    display_menu (todo_list);
}

void clear_tasks (vector<string> &todo_list) {
    todo_list.clear();

    cout << "Tasks cleared." << endl;

    display_menu (todo_list);
}

void export_list (vector<string> &todo_list) {
    ofstream output_file;
    string file_name;

    if (todo_list.size() == 0) {
        cout << "Cannot export empty list." << endl;
    } else {
        cout << "Enter a name for the list: ";
        cin.ignore();
        getline(cin, file_name);

        output_file.open(file_name + ".txt");
        for (string item : todo_list) {
            output_file << item << endl;
        }

        output_file.close();
        cout << "List exported." << endl;
    }

    display_menu (todo_list);
}

void import_list (vector<string> &todo_list) {
    ifstream input_file;
    string entry, file_name;

    cout << "Enter name of list to import: ";
    cin.ignore();
    getline(cin, file_name);

    input_file.open(file_name + ".txt");
    if (!input_file) {
        cout << "File not found." << endl;
    } else {
        todo_list.clear(); // ensures current list is empty before importing another
        while (getline(input_file, entry)) { // reads from file line-by-line
            todo_list.push_back(entry);
        }
        cout << "List imported." << endl;
    }

    input_file.close();

    display_menu (todo_list);    
}

// Pauses program so user can read terminal output, without relying on OS-specific system calls
void end_pause () {
    char ch;
    cout << endl << "Press Enter to continue...";
    cin.ignore();
    cin.get(ch);
}