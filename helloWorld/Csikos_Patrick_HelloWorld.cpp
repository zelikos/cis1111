// Patrick Csikos
// 2020-01-12
// Hello World
// Displays the text "Hello world"

#include <iostream>
using namespace std;

int main()
{
    cout << "Hello world" << endl;
    system("pause");
    return 0;
}