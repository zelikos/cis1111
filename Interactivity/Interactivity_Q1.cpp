#include <iostream>
#include <cmath>
#include <cstdlib>
#include <ctime>
using namespace std;

int main () {
    // Get system time
    int seed = time(0);
    
    // Seed the random number generator
    srand(seed);
    
    // Limit range to be between 1 and 50
    const int MIN_VALUE = 1;
    const int MAX_VALUE = 50;
    
    // Store random numbers
    int number1 = (rand() % (MAX_VALUE - MIN_VALUE + 1)) + MIN_VALUE;
    int number2 = (rand() % (MAX_VALUE - MIN_VALUE + 1)) + MIN_VALUE;
    int number3 = (rand() % (MAX_VALUE - MIN_VALUE + 1)) + MIN_VALUE;
    
    // Print the numbers
    cout << "The numbers are " << number1 << " " << number2 << " " << number3 << endl;
    
    char ch;
    cout << "Press any key to continue...";
    cin.get(ch);
    return 0;
}
