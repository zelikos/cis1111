#include <iostream>
#include <string>
using namespace std;

int main () {
    
    double num1, num2, num3, total;
    double pct_num1, pct_num2, pct_num3;
    cout << "Enter first number: "; cin >> num1;
    cout << "Enter second number: "; cin >> num2;
    cout << "Enter third number: "; cin >> num3;
    total = num1 + num2 + num3;
    pct_num1 = num1 / total;
    pct_num2 = num2 / total;
    pct_num3 = num3 / total;
    
    cout << "Total amount is: " << total << endl << endl;
    
    cout << num1 << " is " << pct_num1 << "% of " << total << endl;
    cout << num2 << " is " << pct_num2 << "% of " << total << endl;
    cout << num3 << " is " << pct_num3 << "% of " << total << endl;
    
    // cout << "Percentages are " << pctS << pctI << pctA << endl;
    
    char ch;
    cout << endl << "Press any key to continue...";
    cin.ignore();
    cin.get(ch);
    return 0;
}
