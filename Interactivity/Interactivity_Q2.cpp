#include <iostream>
using namespace std;

int main () {
    
    string name, address, city;
    cout << "Enter your name: ";
    getline(cin, name);
    cout << "Enter your address: ";
    getline(cin, address);
    cout << "Enter your city: ";
    getline(cin, city);
    cout << endl;
    
    cout << "Name: " << name << endl;
    cout << "Address: " << address << endl;
    cout << "City: " << city << endl << endl;
    
    char ch;
    cout << "Press any key to continue...";
    cin.get(ch);
    return 0;
}
