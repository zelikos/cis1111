// Patrick Csikos
// 2020-04-26
// 1D Arrays
// Manages a list of product codes

#include <iostream>
using namespace std;

void end_pause ();

void show_menu (int arr[], int size, int &num_codes);
void add_codes (int arr[], int size, int &num_codes);
void remove_codes (int arr[], int size, int &num_codes);
void display_codes (int arr[], int size, int &num_codes);


int main () {
    const int ARRAY_SIZE = 1000;
    int product_codes[ARRAY_SIZE] = {}, num_codes = 0;

    show_menu (product_codes, ARRAY_SIZE, num_codes);

    end_pause ();
    return 0;
}

void show_menu (int arr[], int size, int &num_codes) {
    int selection;

    cout << "[1] Add Codes" << endl;
    cout << "[2] Remove Codes" << endl;
    cout << "[3] Display Codes" << endl;
    cout << "[4] Exit" << endl;
    cout << "\n> ";
    cin >> selection;

    switch (selection) {
        case 1:
            add_codes (arr, size, num_codes);
            break;
        case 2:
            remove_codes (arr, size, num_codes);
            break;
        case 3:
            display_codes (arr, size, num_codes);
            break;
        case 4:
            break;
    }
}

void add_codes (int arr[], int size, int &num_codes) {
    int new_code;

    if (num_codes == size) {
        cout << "Database is full. Unable to add codes." << endl;
    } else {
        cout << "Enter product code: ";
        cin >> new_code;
        arr[num_codes] = new_code;
        num_codes++;
    }

    show_menu (arr, size, num_codes);
}

// Pauses program so user can read terminal output, without relying on OS-specific system calls
void end_pause () {
    char ch;
    cout << endl << "Press Enter to continue...";
    cin.ignore();
    cin.get(ch);
}