// Patrick Csikos
// Date
// Program Name
// Program description

#include <iostream>
using namespace std;

void end_pause ();

int main () {
    end_pause ();
    return 0;
}

// Pauses program so user can read terminal output, without relying on OS-specific system calls
void end_pause () {
    char ch;
    cout << endl << "Press Enter to continue...";
    cin.ignore();
    cin.get(ch);
}